## Redisson&Redis Brownbag

[Roberto Palermo](https://www.linkedin.com/in/palermoroberto/)  
*roberto.palermo@flairbit.io*  
<small>03/10/2019</small>

![asd](images/redisson-redis-client.png)


---

### Introduction to Redisson

##### Topics:
- Redis: what is
- Redis installation
- Redis operations
- Redisson: what is
- Configuration
- Distributed objects
- Distributed collections
- Distributed locks
- Redisson Live Objects

[Redisson Reference](https://github.com/redisson/redisson/wiki/Table-of-Content)

---
### What is Redis
Redis is an open source in-memory data structure store.

It supports:

- Various data structures: strings, hashes, lists, sets, sorted sets, ...
- Persistence
- Automatic partitioning: data could be splitted into multiple Redis instances, so that every instance will only contain a subset of keys
- Clustering: data is automatically sharded across multiple Redis nodes, with different degrees of availability during partitions
- Built-in replication: slave Redis instances are exact copies of master instances
- Eviction

[Redis Reference](https://redis.io)

===
### Install Redis

#### Install Redis on Docker
```
docker run --rm -it --name local-redis -p 6379:6379 redis
```

#### Install Homebrew
```
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

#### Install Redis-cli
```
brew tap ringohub/redis-cli

brew update && brew doctor

brew install redis-cli
```


===
### Simple Redis operations
Redis use a key-value method to store the values.

Set a value: 

```
SET name Roberto
```

Get a value: 

```
GET name
```


Check if a key exists: 

```
EXISTS name
```

Delete a key: 

```
DEL name
```

===
### Redis Objects operations: List

Push an element at the head of a list:

```
LPUSH mylist "hello"
```

Push an element at the tail of a list:

```
RPUSH mylist "world"
```

List element of the list in the range:

```
LRANGE mylist 0 1
```

===
### Redis Objects operations: Hash

Set values of an hash map:

```
HMSET user id 100 name "Roberto"
```

Get a field of an hash map:

```
HGET user id  
```

Get an entire hash map:

```
HGETALL user
```

===
### Redis Objects operations: Set

Add elements to a set:

```
SADD mySet 10 45 12
```

Extract all elements from a set:

```
SMEMBERS mySet
```

Verify if the set contains an element:

```
SISMEMBER mySet 10 
```

---

### What is Redisson
Redisson is a Redis client for Java.

<br /><br />
Main features:


- Distributed Java objects and services
- Separation of concern: focus on the data modelling and application logic.
- Works with in-cloud environments: AWS ElastiCache, AWS ElastiCache Cluster, Azure Redis Cache, etc.
- Easy integration with Spring
- Support both synchronous and asynchronous operations

===

### Redisson configuration: Stand-alone
#### Maven dependency

```
<dependency>
	<groupId>org.redisson</groupId>
	<artifactId>redisson</artifactId>
	<version>${redisson.version}</version>
</dependency>
```

#### Java

```
Config config = new Config();
config.useSingleServer()
  .setAddress("redis://localhost:6379");
 
RedissonClient client = Redisson.create(config);
```

===
### Redisson configuration: Spring Boot
#### Maven dependencies

```
<dependency>
	<groupId>org.redisson</groupId>
	<artifactId>redisson</artifactId>
	<version>${redisson.version}</version>
</dependency>
<dependency>
	<groupId>org.redisson</groupId>
	<artifactId>redisson-spring-boot-starter</artifactId>
	<version>${redisson-spring-boot-starter.version}</version>
</dependency>
```

#### Application.properties

```
spring.redis.host=localhost
spring.redis.port=6379
```

#### Java

```
@Autowired
RedissonClient client
```

===
### Redisson distributed objects: Bucket
Redisson could store objects in Redis using the bucket abstraction.
You can set and get buckets using keys.

```
// Get the bucket for the key "anyObject"
RBucket<AnyObject> bucket = redisson.getBucket("anyObject");

// Set an object in the bucket
bucket.set(new AnyObject(1, "First object"));

// Get the object contained in the bucket
AnyObject obj = bucket.get();

// Set a new Object into the bucket only if this is empty
bucket.trySet(new AnyObject(2, "Second object"));

// The object is the first, because trySet didn't change anything
// We can force update of the bucket
bucket.set(new AnyObject(2, "Second object"));

// Now in the bucket there is the second object
// Set a new Object into the bucket, returning the previously contained value
AnyObject previous = bucket.getAndSet(new AnyObject(3, "Third object"));
// Previous object is the second
```

Uses `SET`/`GET` Redis native commands.

===
### Redisson distributed objects: AtomicLong
Redis based distributed `RAtomicLong` object for Java has structure similar to `java.util.concurrent.atomic.AtomicLong` object.


```
RAtomicLong atomicLong = redisson.getAtomicLong("myAtomicLong");
atomicLong.set(3);
System.out.println("atomicLong: " + atomicLong);
atomicLong.incrementAndGet();
System.out.println("atomicLong: " + atomicLong);
atomicLong.decrementAndGet();
System.out.println("atomicLong: " + atomicLong);
```
Uses `INCR`/`DECR` Redis native commands.

<br /><br />
#### Other distributed objects:
* BinaryStreamHolder
* GeospatialHolder
* BitSet
* AtomicLong
* AtomicDouble
* ...

===
### Redisson distributed collections: Map
Redis based distributed `Map` object for Java implements `java.util.concurrent.ConcurrentMap` and `java.util.Map` interfaces. This map keeps elements in insertion order.

```
RMap<String, AnyObject> map = redisson.getMap("anyMap");

// Put an object in the map
AnyObject prevObject = map.put("1", new AnyObject());

// Try to put another object with the same key and retrieve the inserted value
AnyObject previousObject = map.putIfAbsent("1", new AnyObject());

// Not inserted because putIfAbsent works only if there isn't another object with the same key
// Try to insert another object with the same key
previousObject = map.put("1", new AnyObject());

// Now it was inserted
// Remove the object and retrieve it as return value
AnyObject removedObject = map.remove("1");


// Use fast* methods when previous value is not required
map.fastPut("a", new AnyObject());
map.fastPutIfAbsent("d", new AnyObject());
map.fastRemove("a");
```

Uses `HSET`/`HGET` Redis native commands.

===
### Redisson distributed collections: MapCache
`Map` object with eviction support implements `org.redisson.api.RMapCache` interface and extends `java.util.concurrent.ConcurrentMap` interface.

In `MapCache` you can use eviction:

```
RMapCache<String, AnyObject> mapCache = redisson.getMapCache("anyMapCache");
// Add a value with expiration 5 seconds
mapCache.put("key1", new AnyObject(), 5, TimeUnit.SECONDS);
```

You can also define listeners:

```
int expireListener =
    mapCache.addListener(new EntryExpiredListener<String, AnyObject>() {
        @Override
        public void onExpired(EntryEvent<String, AnyObject> event) {
            System.out.println(
                event.getKey() + ": " + event.getValue() + " is expired");
        }
    });
```

Is based on `EXPIRE` Redis native command.

===
### Redisson distributed collections: Set
Redis based `Set` object for Java implements `java.util.Set` interface. Keeps elements uniqueness via element state comparison. `Set` size is limited by Redis to 4 294 967 295 elements.

```
RSet<SomeObject> set = redisson.getSet("anySet");
set.add(new SomeObject());
set.remove(new SomeObject());
```
<br /><br />
also in Set you can define eviction:


```
RSetCache<SomeObject> set = redisson.getSetCache("mySet");

// ttl = 10 minutes, 
set.add(new SomeObject(), 10, TimeUnit.MINUTES);
```

===
### Redisson distributed collections
<br /><br />
* Multimap
* SortedSet
* List
* Queue
* Blocking Queue
* ...

===
### Redisson distributed lock

Redis based distributed reentrant Lock object for Java and implements `java.util.concurrent.locks.Lock` interface.

```
RLock lock = redisson.getLock("anyLock");
lock.lock();
Thread.sleep(5000);
lock.unlock();

// Acquire lock and release it automatically after 5 seconds
// if unlock method hasn't been invoked
lock.lock(5, TimeUnit.SECONDS);
while (lock.isLocked()) {
    Thread.sleep(1000);
    System.out.println("Waiting...");
}
System.out.println("Unlocked!");

// Acquire lock and release it automatically after 5 seconds
// but unlock manually after 2 seconds
lock.lock(5, TimeUnit.SECONDS);
Thread.sleep(2000);
lock.unlock();
```

Uses `SET`/`GET`/`PEXPIRE` Redis native commands.

---
### Redisson Live Objects
#### What is a Live Object

Enhanced version of standard Java object, of which an instance reference can be shared not only between threads in a single JVM, but can also be shared between different JVMs across different machines.
<br /><br /><br />
#### Redisson Live Objects

* All fields inside a Java class are mapped on Redis using hashes
* The `get`/`set` methods of each field are translated to `HGET`/`HSET` commands on the Redis hash (Note that this operations have `O(1)` complexity)
* Since the Redis server is a single-threaded application, all field access to the live objects are atomic


===
### Define the Object

Redisson provides different annotations for Live Object. 

`@RId` and `@REntity` annotations are required to create and use Live Object:

```
@REntity
public class MyObject {

    @RId
    private Integer id;

    private String value;
    private MyObject other;

    public MyObject(Integer id) {
        this.id = id;
    }

    public MyObject() {
    }

    // getters and setters

}
```

===
### RLiveObjectService

To manage the RLOs, we need a `RLiveObjectService`:

```
RLiveObjectService service = redisson.getLiveObjectService();
```

<br><br>
In Spring, you could define it as a bean so that you could inject it where it is required:

```
@Configuration
public class RedisConfig {

    @Bean
    public RLiveObjectService getRedissonLiveObjectService(RedissonClient redisson) {
        return redisson.getLiveObjectService();
    }

}
```
===
### Attach
Best way to obtain a RLO is to use `attach` method:
<br /><br />

```
// Retrieve live object instance
MyObject liveObject = service.attach(new MyObject(1));
```
In this way:
* if a `MyObject` with the same `RId` exists, return a live instance of it
* if it doesn't exists, this method creates it on Redis and returns its live instance

On a Redisson Live Object, you can use setter and getters as for a classic Java Object, but the changes will be applied also on Redis!

```
// Retrieve live object instance
MyObject liveObject = service.attach(new MyObject(1));

// Set the value; it will be persisted on Redis
liveObject.setValue("Value");
```

===
### Nested RLOs

You can also reference to other live objects:

```
// Retrieve another live object instance
MyObject anotherLiveObject = service.attach(new MyObject(2));

// Assign a value to anotherLiveObject
anotherLiveObject.setValue("Another value");
System.out.println("anotherLiveObject: " + anotherLiveObject.toString());

// Reference it into the liveObject instance
liveObject.setOther(anotherLiveObject);
System.out.println("liveObject: " + liveObject.toString());

// Assign a new value to anotherLiveObject
anotherLiveObject.setValue("A new value");

// Retrieve the inner object of the liveObject instance
System.out.println("liveObject.other: " + liveObject.getOther().toString());
// other.value is equal to "A new value"
```

===
### Methods
<br /><br />
Some useful methods provided by the RLiveObjectService are:

* `<T> T merge(T detachedObject)`: transfers all the non null field values to the Redis server. It does not delete any existing data in Redis in case of the field value is null. If this object is not in Redis then a new hash key will be created to store it
* `	<T> T persist(T detachedObject)`: transfers all the non null field values to the Redis server, only when it does not already exist
* `<T> T detach(T attachedObject)`: returns unproxied detached object for the attached object
* `delete(T attachedObject)`: deletes attached object including all nested objects
* `<T> boolean isExists(T instance)`: returns true if the RLiveObject exists in Redis

===
### Delete
<br /><br />
`Delete` method is used to remove the object from Redis, while `isExists` is used to check if an object exists on Redis:

```
// Delete the anotherLiveObject
service.delete(anotherLiveObject);

// Check if exists
System.out.println("isExists anotherLiveObject?: " + service.isExists(anotherLiveObject));

// Print liveObject
System.out.println("liveObject: " + liveObject.toString());
// The "other" field contains an empty object because referenced object was deleted
```

===
### Detach
<br /><br />
`Detach` method is used to obtained an unproxied instance of a Redisson Live Object:

```
// Get a detached instance of the initial liveObject
MyObject detachedObject = service.detach(liveObject);

// Set a new value for the liveObject
liveObject.setValue("New value for liveObject");

// You will see that the detached instance is unchanged
System.out.println("liveObject: " + liveObject.toString());
System.out.println("detachedObject: " + detachedObject.toString());
```

===
### Merge
<br /><br />
`Merge` method transfers all the non null field values to the Redis server. 
It does not delete any existing data in Redis in case of the field value is null. 
If this object is not in Redis then a new hash key will be created to store it:

```
// Create a new MyObject to be merged
MyObject toBeMerged = new MyObject(1);
toBeMerged.setValue("Value to be merged");

// Merge the new MyObject into the Redis one
service.merge(toBeMerged);

// Print liveObject
System.out.println("liveObject: " + liveObject.toString());
// Value is equal to "Value to be merged"
```

===
### Persist
`Persist` method transfers all the non null field values to the Redis server, only when it does not already exist:

```
// Try to persist an object whose id is already contained in Redis
try {
    service.persist(liveObject);
} catch (Exception e) {
    System.out.println("Cannot persist an already existing REntity!");
}
// Create a new MyObject to be persisted
MyObject toBePersisted = new MyObject(3);
toBePersisted.setValue("Value to be persisted");
// Persist it
service.persist(toBePersisted);
// Attach a live object
MyObject liveToBePersisted = service.attach(toBePersisted);
// Try to update the original object
toBePersisted.setValue("New value for toBePersisted object");
// No effects Redis side!
// Try to update the live object
liveToBePersisted.setValue("New value for liveToBePersisted object");
// Now updated on Redis
// If you want the value assigned to the unproxied object, you need to merge it
service.merge(toBePersisted);
// Now you have also on Redis the value you want!
```

===
### Nested collections

Nested collections in Redisson Live Objects are managed using Redis native types:

```
ObjectWithCollections liveObject = service.attach(new ObjectWithCollections(1));
// Try to add an object to the collection valueList
List<String> valueList = liveObject.getValueList();
valueList.add("A value");

// Try to set the list:
liveObject.setValueList(valueList);
System.out.println("liveObject.valueList: " + liveObject.getValueList());

// Try to get the list and invoke the add method directly
liveObject.getValueList().add("Another value");

// Try to use a custom method defined in the object
liveObject.addToValueList("A third value");

```
<br /><br />
In this case, the list is managed as a `RList` and persisted/read with `RPUSH`/`LINDEX` Redis commands.

===
### Index and search

We can also add indexes to the objects in order to search them:

```
@REntity
public class User {

    @RId
    private UUID id = UUID.randomUUID();

    private String name;

    @RIndex
    private Integer age;
}
```

```
service.persist(new User("Luigi", 29));
service.persist(new User("Giovanni", 32));
service.persist(new User("Laura", 22));
service.persist(new User("Giorgia", 38));
Collection<User> users = service.find(User.class, Conditions.lt("age", 30));
// users contain Luigi and Laura
```
<br /><br />
It uses the `ZADD` and `ZRANGEBYSCORE` Redis native functions for indexing the objects.

---
### Conclusions
<br /><br /><br /><br />
* Redis is an open source in-memory data structure store with a lot of features oriented to performances and availability
* Redisson is a Java client for Redis that focus on the data modelling and application logic
* Redisson Live Objects are an enhanced version of standard Java objects, of which an instance reference can easily be shared between different JVMs across different machines


===
<br /><br /><br /><br /><br />
## Thank you for your attention...
## &nbsp; &nbsp; &nbsp; &nbsp; ...and enjoy your meal!