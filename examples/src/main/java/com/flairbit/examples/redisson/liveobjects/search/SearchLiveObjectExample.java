package com.flairbit.examples.redisson.liveobjects.search;

import java.util.Collection;
import com.flairbit.examples.redisson.ConfigUtils;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RLiveObjectService;
import org.redisson.api.RedissonClient;
import org.redisson.api.condition.Conditions;

public class SearchLiveObjectExample {

    public static void main(String[] args) throws Exception {
        RedissonClient redisson = ConfigUtils.buildRedissonClient();
        RLiveObjectService service = redisson.getLiveObjectService();

        service.persist(new User("Luigi", 29));
        service.persist(new User("Giovanni", 32));
        service.persist(new User("Laura", 22));
        service.persist(new User("Giorgia", 38));
        Collection<User> users = service.find(
            User.class, Conditions.lt("age", 30));
        // Users contains Luigi and Laura
        System.out.println("Users with age lower than 30: " + StringUtils.join(users, ","));

    }
}
