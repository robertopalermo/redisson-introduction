package com.flairbit.examples.redisson.distributedobjects;

import com.flairbit.examples.redisson.AnyObject;
import com.flairbit.examples.redisson.ConfigUtils;
import org.redisson.api.RAtomicLong;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;

public class DistributedObjectsExample {

    public static void main(String[] args) throws Exception {
        RedissonClient redisson = ConfigUtils.buildRedissonClient();

        //--- BUCKETS ---

        // Get the bucket for the key "anyObject"
        RBucket<AnyObject> bucket = redisson.getBucket("anyObject");
        // Set an object in the bucket
        bucket.set(new AnyObject(1, "First object"));
        // Get the object contained in the bucket
        AnyObject obj = bucket.get();
        System.out.println("Object in the bucket: " + obj);
        // Set a new Object into the bucket only if this is empty
        bucket.trySet(new AnyObject(2, "Second object"));
        System.out.println("Object in the bucket: " + bucket.get());
        // The object is the first, because trySet didn't change anything
        // We can force update of the bucket
        bucket.set(new AnyObject(2, "Second object"));
        System.out.println("Object in the bucket: " + bucket.get());
        // Now in the bucket there is the second object
        // Set a new Object into the bucket, returning the previously contained value
        AnyObject previous = bucket.getAndSet(new AnyObject(3, "Third object"));
        System.out.println("Object in the bucket: " + bucket.get());
        System.out.println("Previous object: " + previous);

        //--- ATOMIC LONG ---

        RAtomicLong atomicLong = redisson.getAtomicLong("myAtomicLong");
        atomicLong.set(3);
        System.out.println("atomicLong: " + atomicLong);
        atomicLong.incrementAndGet();
        System.out.println("atomicLong: " + atomicLong);
        atomicLong.decrementAndGet();
        System.out.println("atomicLong: " + atomicLong);


    }
}
