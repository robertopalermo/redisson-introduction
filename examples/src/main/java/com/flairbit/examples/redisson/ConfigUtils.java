package com.flairbit.examples.redisson;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;

public class ConfigUtils {

    public static RedissonClient buildRedissonClient() {
        Config config = new Config();
        config.useSingleServer()
            .setAddress("redis://localhost:6379");

        RedissonClient redissonClient = Redisson.create(config);
        redissonClient.getKeys().flushall();
        return redissonClient;
    }
}
