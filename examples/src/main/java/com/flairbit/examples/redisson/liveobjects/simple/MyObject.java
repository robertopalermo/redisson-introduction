package com.flairbit.examples.redisson.liveobjects.simple;

import org.redisson.api.annotation.REntity;
import org.redisson.api.annotation.RId;

@REntity
public class MyObject {

    @RId
    private Integer id;
    private String value;
    private MyObject other;

    public MyObject(Integer id) {
        this.id = id;
    }

    public MyObject() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public MyObject getOther() {
        return other;
    }

    public void setOther(MyObject other) {
        this.other = other;
    }

    @Override
    public String toString() {
        return "MyObject{" + "id='" + getId() + '\'' + ", value='" + getValue() + '\'' + ", other=" + getOther()
            + '}';
    }
}
