package com.flairbit.examples.redisson.distributedcollections;

import java.util.concurrent.TimeUnit;
import com.flairbit.examples.redisson.AnyObject;
import com.flairbit.examples.redisson.ConfigUtils;
import org.redisson.api.RMap;
import org.redisson.api.RMapCache;
import org.redisson.api.RedissonClient;
import org.redisson.api.map.event.EntryEvent;
import org.redisson.api.map.event.EntryExpiredListener;

public class DistributedCollectionsExample {

    public static void main(String[] args) throws Exception {
        RedissonClient redisson = ConfigUtils.buildRedissonClient();

        RMap<String, AnyObject> map = redisson.getMap("anyMap");
        // Put an object in the map
        AnyObject prevObject = map.put("1", new AnyObject());
        System.out.println("anyMap --> 1: " + map.get("1"));
        // Try to put another object with the same key and retrieve the inserted value
        AnyObject previousObject = map.putIfAbsent("1", new AnyObject());
        // Not inserted because putIfAbsent works only if there isn't another object with the same key
        System.out.println("anyMap --> 1: " + map.get("1"));
        System.out.println("previousObject: " + previousObject);
        // Try to insert another object with the same key
        previousObject = map.put("1", new AnyObject());
        System.out.println("anyMap --> 1: " + map.get("1"));
        System.out.println("previousObject: " + previousObject);
        // Now it was inserted
        // Remove the object and retrieve it as return value
        AnyObject removedObject = map.remove("1");
        System.out.println("anyMap --> 1: " + map.get("1"));
        System.out.println("removedObject: " + previousObject);

        // Use fast* methods when previous value is not required
        map.fastPut("a", new AnyObject());
        System.out.println("anyMap --> a: " + map.get("a"));
        map.fastPutIfAbsent("d", new AnyObject());
        System.out.println("anyMap --> d: " + map.get("d"));
        map.fastRemove("a");
        System.out.println("anyMap --> a: " + map.get("a"));

        //--- MAPCACHE ---

        RMapCache<String, AnyObject> mapCache = redisson.getMapCache("anyMapCache");
        // Add a value with expiration 5 seconds
        mapCache.put("key1", new AnyObject(), 5, TimeUnit.SECONDS);
        System.out.println("anyMapCache --> key1: " + mapCache.get("key1"));
        int expireListener =
            mapCache.addListener(new EntryExpiredListener<String, AnyObject>() {
                @Override
                public void onExpired(EntryEvent<String, AnyObject> event) {
                    System.out.println(
                        event.getKey() + ": " + event.getValue() + " is expired");
                }
            });
        Thread.sleep(10000);
        System.out.println("anyMapCache --> key1: " + mapCache.get("key1"));
    }
}
