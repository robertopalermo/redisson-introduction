package com.flairbit.examples.redisson.distributedlock;

import java.util.concurrent.TimeUnit;
import com.flairbit.examples.redisson.ConfigUtils;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;

public class DistributedLockExample {

    public static void main(String[] args) throws Exception {
        RedissonClient redisson = ConfigUtils.buildRedissonClient();

        RLock lock = redisson.getLock("anyLock");
        lock.lock();
        System.out.println("anyLock is locked: " + lock.isLocked());
        Thread.sleep(5000);
        lock.unlock();
        System.out.println("anyLock is locked: " + lock.isLocked());

        // Acquire lock and release it automatically after 5 seconds
        // if unlock method hasn't been invoked
        lock.lock(5, TimeUnit.SECONDS);
        while (lock.isLocked()) {
            Thread.sleep(1000);
            System.out.println("Waiting...");
        }
        System.out.println("Unlocked!");

        // Acquire lock and release it automatically after 5 seconds
        // but unlock manually after 2 seconds
        lock.lock(5, TimeUnit.SECONDS);
        System.out.println("anyLock is locked: " + lock.isLocked());
        Thread.sleep(2000);
        System.out.println("anyLock is locked: " + lock.isLocked());
        lock.unlock();
        System.out.println("anyLock is locked: " + lock.isLocked());
    }

}
