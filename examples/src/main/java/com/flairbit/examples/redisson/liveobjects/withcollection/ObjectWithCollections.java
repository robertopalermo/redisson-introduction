package com.flairbit.examples.redisson.liveobjects.withcollection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.redisson.api.annotation.REntity;
import org.redisson.api.annotation.RId;

@REntity
public class ObjectWithCollections {

    @RId
    private Integer id;
    private List<String> valueList = new ArrayList<>();

    public ObjectWithCollections() {}

    public ObjectWithCollections(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<String> getValueList() {
        return valueList;
    }

    public void setValueList(List<String> valueList) {
        this.valueList = valueList;
    }

    public void addToValueList(String value){
        getValueList().add(value);
    }

    @Override
    public String toString() {
        return "ObjectWithCollections{" + "id=" + id + ", valueList=" + valueList + '}';
    }
}
