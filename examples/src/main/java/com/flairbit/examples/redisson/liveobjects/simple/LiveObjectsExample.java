package com.flairbit.examples.redisson.liveobjects.simple;

import com.flairbit.examples.redisson.ConfigUtils;
import org.redisson.api.RLiveObjectService;
import org.redisson.api.RedissonClient;

public class LiveObjectsExample {

    public static void main(String[] args) throws Exception {
        RedissonClient redisson = ConfigUtils.buildRedissonClient();
        RLiveObjectService service = redisson.getLiveObjectService();

        //--- RLIVEOBJECTS: ATTACH ---

        // Retrieve live object instance
        MyObject liveObject = service.attach(new MyObject(1));
        // Set the value; it will be persisted on Redis
        liveObject.setValue("Value");
        System.out.println("liveObject: " + liveObject.toString());

        //--- RLIVEOBJECTS: NESTED RLOs ---

        // Retrieve another live object instance
        MyObject anotherLiveObject = service.attach(new MyObject(2));
        // Assign a value to anotherLiveObject
        anotherLiveObject.setValue("Another value");
        System.out.println("anotherLiveObject: " + anotherLiveObject.toString());
        // Reference it into the liveObject instance
        liveObject.setOther(anotherLiveObject);
        System.out.println("liveObject: " + liveObject.toString());
        // Assign a new value to anotherLiveObject
        anotherLiveObject.setValue("A new value");
        // Retrieve the inner object of the liveObject instance
        System.out.println("liveObject.other: " + liveObject.getOther().toString());
        // other.value is equal to "A new value"

        //--- RLIVEOBJECTS: DELETE ---

        // Delete the anotherLiveObject
        service.delete(anotherLiveObject);
        // Check if exists
        System.out.println("isExists anotherLiveObject?: " + service.isExists(anotherLiveObject));
        // Print liveObject
        System.out.println("liveObject: " + liveObject.toString());
        // The "other" field contains an empty object because referenced object was deleted

        //--- RLIVEOBJECTS: DETACH ---

        // Get a detached instance of the initial liveObject
        MyObject detachedObject = service.detach(liveObject);
        // Set a new value for the liveObject
        liveObject.setValue("New value for liveObject");
        // You will see that the detached instance is unchanged
        System.out.println("liveObject: " + liveObject.toString());
        System.out.println("detachedObject: " + detachedObject.toString());

        //--- RLIVEOBJECTS: MERGE ---

        // Create a new MyObject to be merged
        MyObject toBeMerged = new MyObject(1);
        toBeMerged.setValue("Value to be merged");
        // Merge the new MyObject into the Redis one
        service.merge(toBeMerged);
        // Print liveObject
        System.out.println("liveObject: " + liveObject.toString());
        // Value is equal to "Value to be merged"

        //--- RLIVEOBJECTS: PERSIST ---

        // Try to persist an object whose id is already contained in Redis
        try {
            service.persist(liveObject);
        } catch (Exception e) {
            System.out.println("Cannot persist an already existing REntity!");
        }
        // Create a new MyObject to be persisted
        MyObject toBePersisted = new MyObject(3);
        toBePersisted.setValue("Value to be persisted");
        // Persist it
        service.persist(toBePersisted);
        // Attach a live object
        MyObject liveToBePersisted = service.attach(toBePersisted);
        System.out.println("toBePersisted: " + toBePersisted);
        System.out.println("liveToBePersisted: " + liveToBePersisted);
        // Try to update the original object
        toBePersisted.setValue("New value for toBePersisted object");
        System.out.println("liveToBePersisted: " + liveToBePersisted);
        // No effects Redis side!
        // Try to update the live object
        liveToBePersisted.setValue("New value for liveToBePersisted object");
        System.out.println("liveToBePersisted: " + liveToBePersisted);
        // Now updated on Redis
        // If you want the value assigned to the unproxied object, you need to merge it
        service.merge(toBePersisted);
        System.out.println("liveToBePersisted: " + liveToBePersisted);
        // Now you have also on Redis the value you want!
    }
}
