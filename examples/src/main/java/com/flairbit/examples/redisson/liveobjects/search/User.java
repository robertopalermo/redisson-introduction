package com.flairbit.examples.redisson.liveobjects.search;

import java.util.UUID;
import org.redisson.api.annotation.REntity;
import org.redisson.api.annotation.RId;
import org.redisson.api.annotation.RIndex;

@REntity
public class User {

    @RId
    private UUID id = UUID.randomUUID();

    private String name;

    @RIndex
    private Integer age;

    public User() {}

    public User(String name, Integer age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", name='" + getName() + '\'' + ", age=" + getAge() + '}';
    }
}
