package com.flairbit.examples.redisson.liveobjects.withcollection;

import java.util.List;
import com.flairbit.examples.redisson.ConfigUtils;
import org.redisson.api.RLiveObjectService;
import org.redisson.api.RedissonClient;

public class LiveObjectWithCollectionExample {

    public static void main(String[] args) throws Exception {
        RedissonClient redisson = ConfigUtils.buildRedissonClient();
        RLiveObjectService service = redisson.getLiveObjectService();

        ObjectWithCollections liveObject = service.attach(new ObjectWithCollections(1));
        // Try to add an object to the collection valueList
        List<String> valueList = liveObject.getValueList();
        valueList.add("A value");
        System.out.println("liveObject.valueList: " + liveObject.getValueList());
        // Try to set the list:
        liveObject.setValueList(valueList);
        System.out.println("liveObject.valueList: " + liveObject.getValueList());
        // Try to get the list and invoke the add method directly
        liveObject.getValueList().add("Another value");
        System.out.println("liveObject.valueList: " + liveObject.getValueList());
        // Try to use a custom method defined in the object
        liveObject.addToValueList("A third value");
        System.out.println("liveObject.valueList: " + liveObject.getValueList());
    }
}
