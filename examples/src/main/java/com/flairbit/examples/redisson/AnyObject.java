package com.flairbit.examples.redisson;

import java.io.Serializable;
import java.util.Random;
import com.github.javafaker.Faker;

public class AnyObject implements Serializable {

    private Integer id;
    private String name;

    public AnyObject(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public AnyObject() {
        this.id = new Random().nextInt();
        this.name = new Faker().name().fullName();
    }

    @Override
    public String toString() {
        return "AnyObject{" + "id=" + id + ", name='" + name + '\'' + '}';
    }
}
